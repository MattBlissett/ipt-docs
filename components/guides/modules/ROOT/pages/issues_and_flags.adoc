= Issues and flags

More than *50 issues and flags* have been created to deal with common
data quality problems.

.Geospatial issues
[cols="2,6,1,1"]
|===
|Flag name | Definition | Terms | Example

|*Zero coordinate*
|Coordinates are exactly 0/0, often indicating an actual null coordinate.
|_dwc:decimalLatitude_, _dwc:decimalLongitude_
|https://www.gbif.org/occurrence/search?issue=ZERO_COORDINATE[example]

|*Coordinate out of range*
|The supplied coordinates lie outside of the range for decimal lat/lon values (-90/90, -180/180).
|_dwc:decimalLatitude_, _dwc:decimalLongitude_, _dwc:verbatimCoordinates_, _dwc:verbatimLatitude_, _dwc:verbatimLongitude_
|https://www.gbif.org/occurrence/search?issue=COORDINATE_OUT_OF_RANGE[example]

|*Country coordinate mismatch*
|The interpreted occurrence coordinates fall outside of the indicated country.
|_dwc:countryCode_, _dwc:country_, _dwc:decimalLatitude_
|https://www.gbif.org/occurrence/search?issue=COUNTRY_COORDINATE_MISMATCH[example]

|*Coordinate invalid*
|A coordinate value is given in some form, but GBIF is unable to interpret it. Possible reasons include, i.a., coordinates that fall out of range(larger/lower than 90/-90 or 180/-180, depending) or text values that cannot be interpreted.
|_dwc:decimalLatitude_, _dwc:decimalLongitude_, _dwc:verbatimCoordinates_, _dwc:verbatimLatitude_, _dwc:verbatimLongitude_
|https://www.gbif.org/occurrence/search?issue=COORDINATE_INVALID[example]

|*Geodetic datum assumed WGS84*
|If the datum is null, data interpretation assumes the record coordinates are in WGS84.
|_dwc:geodeticDatum_
|https://www.gbif.org/occurrence/search?issue=GEODETIC_DATUM_ASSUMED_WGS84[example]

|*Geodetic datum invalid*
|The geodetic datum could not be interpreted, because the supplied term cannot be matched against the vocabulary of known values.
|_dwc:geodeticDatum_
|https://www.gbif.org/occurrence/search?issue=GEODETIC_DATUM_INVALID[example] +

|*Country mismatch*
|Interpreted Country and Country code contradict each other.
|_dwc:countryCode_, _dwc:country_
|https://www.gbif.org/occurrence/search?issue=COUNTRY_MISMATCH[example]

|*Country derived from coordinates*
|If the country and country code are not supplied or cannot be matched to known values, data interpretation derives their content from the decimal coordinates through a https://github.com/gbif/geocode[lookup service].
|_dwc:countryCode_, _dwc:country_, _dwc:decimalLatitude_, _dwc:decimalLongitude_
|https://www.gbif.org/occurrence/search?issue=COUNTRY_DERIVED_FROM_COORDINATES[example]

|*Country invalid*
|The country or countryCode given cannot be matched to the vocabulary for country names.
|_dwc:country_
|https://www.gbif.org/occurrence/search?issue=COUNTRY_INVALID[example]

|*Continent invalid*
|The continent given cannot be matched to the vocabulary for continent names.
|_dwc:continent_
|https://www.gbif.org/occurrence/search?issue=CONTINENT_INVALID[example]

|*Coordinate rounded*
|In the data interpretation the original coordinates are rounded to 6 decimals (~1m precision).
|_dwc:decimalLatitude_, _dwc:decimalLongitude_
|https://www.gbif.org/occurrence/search?issue=COORDINATE_ROUNDED[example]

|*Coordinate reprojected*
|The original coordinates were successfully reprojected from a different geodetic datum to WGS84.
|_dwc:geodeticDatum_
|https://www.gbif.org/occurrence/search?issue=COORDINATE_REPROJECTED[example]

|*Coordinate reprojection suspicious*
|Indicates successful coordinate reprojection according to provided datum, but which results in a datum shift larger than 0.1 decimal degrees.
|_dwc:geodeticDatum_, _dwc:decimalLatitude_, _dwc:decimalLongitude_
|https://www.gbif.org/occurrence/search?issue=COORDINATE_REPROJECTION_SUSPICIOUS[example]

|*Coordinate reprojection failed*
|The given decimal latitude and longitude could not be reprojected to WGS84 based on the provided datum.
|_dwc:geodeticDatum_, _dwc:decimalLatitude_, _dwc:decimalLongitude_
|https://www.gbif.org/occurrence/search?issue=COORDINATE_REPROJECTION_FAILED[example]

|*Coordinate uncertainty meters invalid*
|The value given for Coordinate uncertainty in meters, indicating the radius of uncertainty around the given decimal coordinates, is not a valid number, or lies outside a plausible range.
|_dwc:coordinateUncertaintyInMeters_
|https://www.gbif.org/occurrence/search?issue=COORDINATE_UNCERTAINTY_METERS_INVALID[example]

|*Coordinate precision invalid*
|Indicates an invalid or very unlikely coordinates precision. The value is not a decimal number as expected, or it has an unusually low or high for a margin of uncertainty.
|_dwc:coordinatePrecision_
|https://www.gbif.org/occurrence/search?issue=COORDINATE_PRECISION_INVALID[example]

|*Presumed negated longitude*
|The supplied longitude value places the coordinates outside of the indicated country. Negating the longitude value would result in a country match.
|_dwc:decimalLongitude_
|https://www.gbif.org/occurrence/search?issue=PRESUMED_NEGATED_LONGITUDE[example]

|*Presumed negated latitude*
|The supplied latitude value places the coordinates outside of the indicated country. Negating the latitude value would result in a country match.
|_dwc:decimalLatitude_
|https://www.gbif.org/occurrence/search?issue=PRESUMED_NEGATED_LATITUDE[example]

|*Presumed swapped coordinate*
|Coordinates seem to be swapped when testing against the interpreted country.
|_dwc:decimalLatitude_, _dwc:decimalLongitude_, _dwc:country_
|https://www.gbif.org/occurrence/search?issue=PRESUMED_SWAPPED_COORDINATE[example]

|*Depth min max swapped*
|The values for minimum and maximum depth appear to the swapped.
|_dwc:minimumDepthInMeters_, _dwc:maximumDepthInMeters_
|https://www.gbif.org/occurrence/search?issue=DEPTH_MIN_MAX_SWAPPED[example]

|*Depth non numeric*
|The values for minimum and maximum depth are non-numeric values and cannot be interpreted.
|_dwc:minimumDepthInMeters_, _dwc:maximumDepthInMeters_
|https://www.gbif.org/occurrence/search?issue=DEPTH_NON_NUMERIC[example]

|*Depth unlikely*
|The values for minimum and maximum depth are negative or higher than 11000 (Mariana Trench depth in meters).
|_dwc:minimumDepthInMeters_, _dwc:maximumDepthInMeters_
|https://www.gbif.org/occurrence/search?issue=DEPTH_UNLIKELY[example]

|*Depth not metric*
|Set if supplied depth is not given in the metric system, for example using feet instead of meters.
|_dwc:minimumDepthInMeters_, _dwc:maximumDepthInMeters_
|https://www.gbif.org/occurrence/search?issue=DEPTH_NOT_METRIC[example]

|*Elevation non numeric*
|The values for minimum and maximum elevation are non-numeric values and cannot be interpreted.
|_dwc:minimumElevationInMeters_, _dwc:maximumElevationMeters_
|https://www.gbif.org/occurrence/search?issue=ELEVATION_NON_NUMERIC[example]

|*Elevation min max swapped*
|The values for minimum and maximum elevation appear to the swapped.
|_dwc:minimumElevationInMeters_, _dwc:maximumElevationInMeters_
|https://www.gbif.org/occurrence/search?issue=ELEVATION_MIN_MAX_SWAPPED[example]

|*Elevation not metric*
|Set if supplied elevation is not given in the metric system, for example using feet instead of meters.
|_dwc:minimumElevationInMeters_, _dwc:maximumElevationInMeters_
|https://www.gbif.org/occurrence/search?issue=ELEVATION_NOT_METRIC[example]

|*Elevation unlikely*
|The values for minimum and maximum elevation are above the troposphere (17000 m) or below Mariana Trench (11000 m).
|_dwc:minimumElevationInMeters_, _dwc:maximumElevationInMeters_
|https://www.gbif.org/occurrence/search?issue=ELEVATION_UNLIKELY[example]

|*Continent country mismatch*
|The interpreted continent and country do not match up.
|_dwc:continent_, _dwc:countryCode_, _dwc:country_
|https://www.gbif.org/occurrence/search?issue=CONTINENT_COUNTRY_MISMATCH[example]

|*Continent derived from coordinates*
|If no value is supplied for the continent or if the values cannot be matched against a known vocabulary, data interpretation derives the continent from the decimal coordinates.
|_dwc:continent_, _dwc:decimalLatitude_, _dwc:decimal Longitude_
|https://www.gbif.org/occurrence/search?issue=CONTINENT_DERIVED_FROM_COORDINATES[example]
|===

.Taxonomic issues
[cols="2,6,1,1"]
|===
|Flag name | Definition | Terms | Example

|*Taxon match higherrank*
a|The record can be matched to the GBIF taxonomic backbone at a higher rank, but not with the scientific name given.

Reasons include:

* The name is new, and not available in the taxonomic datasets yet
* The name is missing in the backbone's taxonomic sources for others reasons
* Formatting or spelling of the scientific name caused interpretation errors
|_dwc:scientificName, dwc:kingdom, dwc:phylum, dwc:class, dwc:order, dwc:family, dwc:genus, dwc:subgenus, dwc:specificEpithet, dwc:infraspecificEpithet, dwc:taxonRank_
|https://www.gbif.org/occurrence/search?issue=TAXON_MATCH_HIGHERRANK[example]

|*Taxon match none*
|Matching to the taxonomic backbone cannot be done cause there was no match at all or several matches with too little information to keep them apart(homonyms).
|_dwc:scientificName, dwc:kingdom, dwc:phylum, dwc:class, dwc:order, dwc:family, dwc:genus, dwc:subgenus, dwc:specificEpithet, dwc:infraspecificEpithet, dwc:taxonRank_
|https://www.gbif.org/occurrence/search?issue=TAXON_MATCH_NONE[example]

|*Taxon match fuzzy*
|Matching to the taxonomic backbone can only be done using a fuzzy, non exact match.
|dwc:scientificName, dwc:kingdom, dwc:phylum, dwc:class, dwc:order, dwc:family, dwc:genus, dwc:subgenus, dwc:specificEpithet, dwc:infraspecificEpithet, dwc:taxonRank
|https://www.gbif.org/occurrence/search?issue=TAXON_MATCH_FUZZY[example]

|===

.Date issues
[cols="2,6,1,1"]
|===
|Flag name | Definition | Terms | Example

|*Recorded date invalid*
a|The recording date given cannot be intrepreted because is invalid.

Reasons include:

* A non-existing date (e.g "1995-04-34")
* Missing date parts (e.g. Event date without year).
* The date format does not follow the ISO 8601 standard (YYYY-MM-DD)
|_dwc:eventDate, dwc:year, dwc:month, dwc:day_
|https://www.gbif.org/occurrence/search?issue=RECORDED_DATE_INVALID[example]

|*Recorded date mismatch*
|The recording date specified as the eventDate string and the individual year, month, day are contradicting.
|_dwc:eventDate, dwc:year, dwc:month, dwc:day_
|https://www.gbif.org/occurrence/search?issue=RECORDED_DATE_MISMATCH[example]

|*Identified date unlikely*
|The identification date is in the future or before Linnean times (1700).
|_dwc:dateIdentified_
|https://www.gbif.org/occurrence/search?issue=IDENTIFIED_DATE_UNLIKELY[example]

|*Recorded Date Unlikely*
|The recording date is highly unlikely, falling either into the future or representing a very old date before 1600 that predates modern taxonomy.
|_dwc:eventDate, dwc:year, dwc:month, dwc:day_
|https://www.gbif.org/occurrence/search?issue=RECORDED_DATE_UNLIKELY[example]

|*Multimedia date invalid*
a|The creation date given cannot be intrepreted because is invalid.

Reasons include:

* A non-existing date (e.g "1995-04-34")
* Missing date parts (e.g. Event date without year)
* The date format does not follow the ISO 8601 standard (YYYY-MM-DD)
|_dwc:created_
|https://www.gbif.org/occurrence/search?issue=MULTIMEDIA_DATE_INVALID[example]

|*Identified date invalid*
a|The identification date given cannot be intrepreted because is invalid.

Reasons include:

* A non-existing date (e.g "1995-04-34")
* Missing date parts (e.g. without year)
* The date format does not follow the ISO 8601 standard (YYYY-MM-DD)
|_dwc:dateIdentified_
|https://www.gbif.org/occurrence/search?issue=IDENTIFIED_DATE_INVALID[example]

|*Modified date invalid*
a|A (partial) invalid modified date is given.

Reasons include:

* A non-existing date (e.g "1995-04-34")
* Missing date parts (e.g. without year)
* The date format does not follow the ISO 8601 standard (YYYY-MM-DD)
|_dc:modified_
|https://www.gbif.org/occurrence/search?issue=MODIFIED_DATE_INVALID[example]

|*Modified date unlikely*
|The modified date given is in the future or predates unix time (1970).
|_dc:modified_
|https://www.gbif.org/occurrence/search?issue=MODIFIED_DATE_UNLIKELY[example]

|*Georeferenced date invalid* (date)
a|The georeference date given cannot be interpreted because it is invalid.

Reasons include:

* A non-existing date (e.g "1995-04-34") +
* Missing date parts (e.g. without year) +
* The date format does not follow the ISO 8601 standard (YYYY-MM-DD)
|_dwc:georeferencedDate_
|https://www.gbif.org/occurrence/search?issue=GEOREFERENCED_DATE_INVALID[example]

|*Georeferenced date unlikely*
|The georeference date given is in the future or before Linnean times (1700).
|_dwc:georeferencedDate_
|https://www.gbif.org/occurrence/search?issue=GEOREFERENCED_DATE_UNLIKELY[example]
|===

.Vocabulary issues
[cols="2,6,1,1"]
|===
|Flag name | Definition | Terms | Example

|*Basis of record invalid*
|The given basis of record is impossible to interpret or very different from the recommended vocabulary: http://rs.gbif.org/vocabulary/dwc/basis_of_record.xml[http://rs.gbif.org/vocabulary/dwc/basis_of_record.xml]
|_dwc:basisOfRecord_
|https://www.gbif.org/occurrence/search?issue=BASIS_OF_RECORD_INVALID[example]

|*Type status invalid*
|The given type status is impossible to interpret or very different from the recommended vocabulary: https://rs.gbif.org/vocabulary/gbif/type_status.xml[https://rs.gbif.org/vocabulary/gbif/type_status.xml]
|_dwc:typeStatus_
|https://www.gbif.org/occurrence/search?issue=TYPE_STATUS_INVALID[example]

|*Occurrence status unparsable*
|The given occurenceStatus value cannot be interpreted; it does not match any of the known (vocabulary) values that indicate the presence or absence of a species at or observation event.
|_dwc:occurrenceStatus_
|https://www.gbif.org/occurrence/search?issue=OCCURRENCE_STATUS_UNPARSABLE[example]

|===

.GRSciColl issues
[cols="2,6,1,1"]
|===
|Flag name | Definition | Terms | Example

|*Ambiguous institution*
|Multiple institutions were found in https://www.gbif.org/grscicoll[GRSciColl] with the same level of confidence and it can't be determined which one should be accepted. For example, there are several institutions with the same code and country. See https://www.gbif.org/faq?question=how-can-i-improve-the-matching-of-occurrence-records-with-grscicoll[this FAQ] on how to avoid ambiguous matches.
|_dwc:institutionCode, dwc:institutionID_
|https://www.gbif.org/occurrence/search?issue=AMBIGUOUS_INSTITUTION[example]

|*Ambiguous collection*
|Multiple collections were found in https://www.gbif.org/grscicoll[GRSciColl] with the same level of confidence and it can't be determined which one should be accepted. For example, there are several collections belonging to the same institution with the same code. See https://www.gbif.org/faq?question=how-can-i-improve-the-matching-of-occurrence-records-with-grscicoll[this FAQ] on how to avoid ambiguous matches.
|_dwc:collectionCode, dwc:collectionID_
|https://www.gbif.org/occurrence/search?issue=AMBIGUOUS_COLLECTION[example]

|*Institution match none*
|No match was found in https://www.gbif.org/grscicoll[GRSciColl]. Either the entry doesn't exists in GRSciColl or it has a different code. Check https://www.gbif.org/grscicoll[GRSciColl] and request update if needed.
|_dwc:institutionCode, dwc:institutionID_
|https://www.gbif.org/occurrence/search?issue=INSTITUTION_MATCH_NONE[example]

|*Collection match none*
|No match was found in https://www.gbif.org/grscicoll[GRSciColl]. Either the entry doesn't exists in GRSciColl or it has a different code. Check https://www.gbif.org/grscicoll[GRSciColl] and request update if needed.
|_dwc:collectionCode, dwc:collectionID_
|https://www.gbif.org/occurrence/search?issue=COLLECTION_MATCH_NONE[example]

|*Institution match fuzzy*
|A match was found in https://www.gbif.org/grscicoll[GRSciColl] but it was matched fuzzily. To know more about why this has happened you can use the https://www.gbif.org/developer/registry#lookup[lookup API] to see see the "reasons" returned in the response. A common case is when the name is used instead of the code or the identifier. To avoid fuzzy matches, publishers should use identifiers in additon to codes. More details available in https://www.gbif.org/faq?question=how-can-i-improve-the-matching-of-occurrence-records-with-grscicoll[this FAQ].
|_dwc:institutionCode, dwc:institutionID_
|https://www.gbif.org/occurrence/search?issue=INSTITUTION_MATCH_FUZZY[example]

|*Collection match fuzzy*
|A match was found in https://www.gbif.org/grscicoll[GRSciColl] but it was matched fuzzily. To know more about why this has happened you can use the https://www.gbif.org/developer/registry#lookup[lookup API] to see the "reasons" returned in the response. A common case is when the name is used instead of the code or the identifier. To avoid fuzzy matches, publishers should use identifiers in additon to codes. More details available in https://www.gbif.org/faq?question=how-can-i-improve-the-matching-of-occurrence-records-with-grscicoll[this FAQ].
|_dwc:collectionCode, dwc:collectionID_
|https://www.gbif.org/occurrence/search?issue=COLLECTION_MATCH_FUZZY[example]

|*Institution collection mismatch*
|At least one possible collection match was found in https://www.gbif.org/grscicoll[GRSciColl] but none of them belong to the institution matched.
|_dwc:collectionCode, dwc:collectionID, dwc:institutionCode, dwc:institutionID_
|https://www.gbif.org/occurrence/search?issue=INSTITUTION_COLLECTION_MISMATCH[example]

|*Different owner institution*
|The institution doesn't match the owner institution.
|_dwc:ownerInstitutionCode, dwc:institutionCode, dwc:institutionID_
|https://www.gbif.org/occurrence/search?issue=DIFFERENT_OWNER_INSTITUTION[example]

|===

.Other issues
[cols="2,6,1,1"]
|===
|Flag name | Definition | Terms | Example

|*Individual count invalid*
|Individual count value not parsable into a positive integer.
|_dwc:individualCount_
|https://www.gbif.org/occurrence/search?issue=INDIVIDUAL_COUNT_INVALID[example]

|*Individual count conflicts with occurrence status*
|The values given for the individual count and for the status of the occurrence (present/absent) contradict each other (e.g. the count is 0 but the status says "present").
|_dwc:individualCount, dwc:occurrenceStatus_
|https://www.gbif.org/occurrence/search?issue=INDIVIDUAL_COUNT_CONFLICTS_WITH_OCCURRENCE_STATUS[example]

|*Occurrence status inferred from individual count*
|The present/absent status of the occurrence was inferred from the individual count value because no status value was supplied explicitly. An individual count of 0 is interpreted as status="absent", a value > 0 as "present".
|_dwc:individualCount, dwc:occurrenceStatus_
|https://www.gbif.org/occurrence/search?issue=OCCURRENCE_STATUS_INFERRED_FROM_INDIVIDUAL_COUNT[example]

|*References URI invalid*
|The references URL cannot be resolved, and may be malformed or contain invalid characters.
|_dc:references_
|https://www.gbif.org/occurrence/search?issue=REFERENCES_URI_INVALID[example]

|*Multimedia URI invalid*
|The multimedia URL cannot be resolved, and may be malformed or contain invalid characters.
|_dwc:associatedMedia_
|https://www.gbif.org/occurrence/search?issue=MULTIMEDIA_URI_INVALID[example]

|*Interpretation error*
|An error occurred during interpretation, leaving the record interpretation incomplete.
|
|https://www.gbif.org/occurrence/search?issue=INTERPRETATION_ERROR[example]

|==
